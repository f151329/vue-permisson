const express = require("express");
const app = express();
const cors = require("cors");
const morgan = require("morgan");
const adminData = require("./data/admin_login.json");
const vipData = require("./data/vip_login.json");
const adminPremission = require("./data/admin_premisson.json");
const vipPremission = require("./data/vip_premisson.json");
app.use(express.urlencoded());
app.use(express.json());
app.use(cors());
app.use(morgan("dev"));
app.post("/login", function (req, res) {
  const userType = req.body;
  console.log(userType);
  if (userType.username === "admin") {
    res.send(adminData);
  } else {
    res.send(vipData);
  }
});
app.post("/premission", function (req, res) {
  const token = req.body.token;
  console.log(req.body);
  if (token === "admin") {
    res.send(adminPremission);
  } else {
    res.send(vipPremission);
  }
});
app.listen(3000, () => {
  console.log("http://localhost:3000");
});
