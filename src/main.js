import Vue from 'vue'
import App from './App.vue' // 根组件
import './assets/css/global.css'
import VueRouter from 'vue-router' // 路有插件
import router from './router' // 路由实例
import './plugins/element.js'

import store from './store'
import './until/http'
Vue.config.productionTip = false
// 解决Vue-Router升级导致的Uncaught(in promise) navigation guard问题
// push
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location, onResolve, onReject) {
  if (onResolve || onReject)
    return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}

// replace
const originalReplace = VueRouter.prototype.replace
VueRouter.prototype.replace = function replace(location) {
  return originalReplace.call(this, location).catch(err => err)
}
Vue.use(VueRouter)
Vue.use(VueRouter) // vue实例使用vue-router插件

new Vue({
  render: h => h(App),
  store,
  router, // 注册路由实例
}).$mount('#app')
