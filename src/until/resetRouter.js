import VueRouter from 'vue-router'
import { initRoutes } from '../router'
export default function resetRouter() {
  const newRouter = new VueRouter({
    routes: initRoutes, //引入的公共路由
  })
  VueRouter.matcher = newRouter.matcher // reset router
}
