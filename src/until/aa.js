function createRouter(userRoutes = [], allRoutes = []) {
  let route = []
  allRoutes.forEach((allItem, index) => {
    //1. 遍历所有路由
    userRoutes.forEach((userItem, i) => {
      //2. 遍历后台返回的路由
      //3. 使用所有路由的name在后台返回的路由中查找
      //   如果找到就说明存在,否则直接将该节点返

      if (userItem.name === allItem.meta.name) {
        //3.2接着判断如果所有路由中有children节点,则说明该路由有子元素
        //    接着递归调用,将用户路由和所有路由的children属性传入
        if (userItem.children && userItem.children.length > 0) {
          allItem.children = createRouter(userItem.children, allItem.children)
        }
        route.push(allItem)
      }
    })
  })
  return route
}
const allRoutes = [
  {
    path: '/home',

    meta: {
      name: '主页',
      icon: 'el-icon-home',
    },
  },
  {
    path: '/user',

    meta: {
      name: '用户管理',
      icon: 'el-icon-user-solid',
    },
    children: [
      {
        path: '/userEdit',

        meta: {
          name: '用户编辑',
          icon: 'el-icon-edit',
        },
      },
      {
        path: '/userList',

        meta: {
          name: '用户列表',
          icon: 'el-icon-open',
        },
      },
    ],
  },
  {
    path: '/order',

    meta: {
      name: '订单管理',
      icon: 'el-icon-platform-eleme',
    },
    children: [
      {
        path: '/orderBack',

        meta: {
          name: '订单退货',
          icon: 'el-icon-circle-close',
        },
      },
      {
        path: '/orderCreate',

        meta: {
          name: '订单创建',
          icon: 'el-icon-circle-plus-outline',
        },
      },
      {
        path: '/orderVerfity',

        meta: {
          name: '订单审核',
          icon: 'el-icon-aim',
        },
        children: [
          {
            path: '/orderPassList',

            meta: {
              name: '审核通过列表',
              icon: 'el-icon-check',
            },
          },
          {
            path: '/orderNoPassList',

            meta: {
              name: '审核未通过列表',
              icon: 'el-icon-close',
            },
          },
        ],
      },
    ],
  },
]
const admin = [
  {
    name: '订单管理',
    children: [
      {
        name: '订单创建',
      },
      {
        name: '订单审核',
        children: [
          {
            name: '审核通过列表',
          },
          {
            name: '审核未通过列表',
          },
        ],
      },
      {
        name: '订单退货',
      },
    ],
  },
  {
    name: '用户管理',
    children: [
      {
        name: '用户列表',
      },
      {
        name: '用户编辑',
      },
    ],
  },
]
const vip = [
  {
    name: '订单管理',
    children: [
      {
        name: '订单创建',
      },
    ],
  },
]
const aa = createRouter(admin, allRoutes)
console.dir(aa)
