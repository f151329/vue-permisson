import vue from "vue";
import axios from "axios";
import store from "../store/";
axios.defaults.baseURL = "http://localhost:3000";
axios.interceptors.request.use(
  (config) => {
    const token = store.state.token;

    if (token) return (config.headers.Authorization = token);
    return config;
  },
  (err) => {
    Promise.reject(err);
  }
);
vue.prototype.$http = axios;
export default axios;
