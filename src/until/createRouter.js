// export default function createRouter(userRoutes = [], allRoutes = []) {
//   let realRoutes = []
//   allRoutes.forEach(allRoutesItem => {
//     userRoutes.forEach(userRoutesItem => {
//       if (userRoutesItem.name === allRoutesItem.meta.name) {
//         debugger
//         if (userRoutesItem.children && userRoutesItem.children.length > 0) {
//           allRoutesItem.children = createRoutes(
//             userRoutesItem.children,
//             allRoutesItem.children
//           )
//         }
//         realRoutes.push(allRoutesItem)
//       }
//     })
//   })
//   return realRoutes
// }
// export default function createRouter(userRoutes = [], allRoutes = []) {
//   let realRoutes = []
//   allRoutes.forEach((v, i) => {
//     userRoutes.forEach((item, index) => {
//       if (item.name === v.meta.name) {
//         if (item.children && item.children.length > 0) {
//           v.children = createRoutes(item.children, v.children)
//         }
//         realRoutes.push(v)
//       }
//     })
//   })
//   return realRoutes
// }
export default function createRouter(userRoutes = [], allRoutes = []) {
  console.log('传入的值allRoutes:', allRoutes)
  const route = []
  allRoutes.forEach((allItem, index) => {
    //1. 遍历所有路由
    userRoutes.forEach((userItem, i) => {
      //2. 遍历后台返回的路由
      //3. 使用所有路由的name在后台返回的路由中查找
      //   如果找到就说明存在,否则直接将该节点返
      var node = allItem
      if (userItem.name === allItem.meta.name) {
        //3.2接着判断如果所有路由中有children节点,则说明该路由有子元素
        //    接着递归调用,将用户路由和所有路由的children属性传入
        if (userItem.children && userItem.children.length > 0) {
          node.children = createRouter(userItem.children, allItem.children)
        }
        route.push(node)
      }
    })
  })
  return route
}
