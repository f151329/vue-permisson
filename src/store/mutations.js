export default {
  LOGIN_IN(state, token) {
    state.userToken = token
  },
  LOG_OUT(state) {
    state.userToken = ''
  },
  SET_PERMISSION(state, payload) {
    state.permissionList = payload
  },
  ClEAN_PERMISSION(state) {
    state.permissionList = null
  },
  SET_MENU(state, menuList) {
    state.menuList = menuList
  },
  ClEAN_MENU(state) {
    state.menuList = null
  },
  ADD_COUNT(state) {
    state.count += 1
  },
  ClEAN_COUNT(state) {
    state.count = 0
  },
}
