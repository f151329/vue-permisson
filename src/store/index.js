import vuex from "vuex";
import vue from "vue";
vue.use(vuex);
import state from "./defaultState";
import mutations from "./mutations";
import actions from "./actions";
const store = new vuex.Store({
  state,
  mutations,
  actions,
});
export default store;
