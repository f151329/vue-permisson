export default {
  get userToken() {
    return localStorage.getItem('token')
  },
  set userToken(value) {
    localStorage.setItem('token', value)
  },
  count: 0,
  permissionList: null,
  menuList: null,
}
