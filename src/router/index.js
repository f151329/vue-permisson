import VueRouter from 'vue-router'
import login from '../views/login/login.vue'

import layout from '../views/layout/layout.vue'
import store from '../store'
import allroutes1 from './allRoutes1'
console.log(allroutes1)

export const initRoutes = [
  {
    path: '/',
    redirect: '/login',
  },
  {
    path: '/login',
    name: 'login',
    component: login,
  },
]
export const DynamicRoutes = [
  {
    path: '/layout',
    component: layout,
    redirect: '/order',
    children: [],
  },
]

// DynamicRoutes[0].children.push(...allroute)
// 创建路由实例
const router = new VueRouter({
<<<<<<< HEAD
  // routes: [...initRoutes, ...DynamicRoutes],//这是将所有路由写死
  routes: initRoutes,
=======
  // routes: [...initRoutes, ...DynamicRoutes],
  routes: [...initRoutes],
  // routes: allStaticRoutes,
>>>>>>> 271dfd4681648e3a213e8ac1a89a09025dcf0f94
})
router.beforeEach((to, from, next) => {
  console.log(to.path)
  const token = localStorage.getItem('token')
  //将从初始的响应式路由表存储起来
  if (!localStorage.getItem('allRoutes')) {
    const staicRoutes = JSON.stringify(allroutes1)
    delete staicRoutes.component
    console.log(staicRoutes)
    localStorage.setItem('allRoutes', staicRoutes)
  }
  // 1.to的是不是登录页，是的话直接放行，不进行判断
  if (to.path === '/login') {
    next()
  } else {
    // 2.说明不是登录页，判断有没有token
    if (token) {
      // 2.1 有token，已经进行了登录，使用获取的token得到用户权限列表
      // 3 判断有没有获取到权限
      const permissionList = store.state.permissionList
      console.log('list:', permissionList)
      if (permissionList) {
        // 3.1获取到了权限
        next()
      } else {
        // 3.2没有获取到了权限，重新进行登录
        store.dispatch('getUserInfo').then(() => {
          next({
            path: to.path,
          })
        })

        console.log('beforeeach获取的权限', permissionList)
      }
    } else {
      // 2.2 没有token，也就不用再判断权限，说明登录失效，跳转到登录页重新登录
      next('/login')
    }
  }
})
export default router
