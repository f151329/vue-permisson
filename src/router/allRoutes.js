// 用户菜单相关
import user from '../views/user'
import userList from '../views/user/userManager/userList.vue'
import userEdit from '../views/user/userManager/userEdit.vue'
// 订单管理相关
import order from '../views/order'
import orderBack from '../views/order/orderBack.vue'
import orderCreate from '../views/order/orderCreate.vue'
import orderVerfity from '../views/order/orderVerfity'
import orderPassList from '../views/order/orderVerfity/orderPassList.vue'
import orderNoPassList from '../views/order/orderVerfity/orderNoPassList.vue'
import home from '../views/home/home.vue'
const allRoutes = [
  {
    path: '/home',
    component: home,
    meta: {
      name: '主页',
      icon: 'el-icon-home',
    },
  },
  {
    path: '/user',
    component: user,
    meta: {
      name: '用户管理',
      icon: 'el-icon-user-solid',
    },
    children: [
      {
        path: '/userEdit',
        component: userEdit,
        meta: {
          name: '用户编辑',
          icon: 'el-icon-edit',
        },
      },
      {
        path: '/userList',
        component: userList,
        meta: {
          name: '用户列表',
          icon: 'el-icon-open',
        },
      },
    ],
  },
  {
    path: '/order',
    component: order,
    meta: {
      name: '订单管理',
      icon: 'el-icon-platform-eleme',
    },
    children: [
      {
        path: '/orderBack',
        component: orderBack,
        meta: {
          name: '订单退货',
          icon: 'el-icon-circle-close',
        },
      },
      {
        path: '/orderCreate',
        component: orderCreate,
        meta: {
          name: '订单创建',
          icon: 'el-icon-circle-plus-outline',
        },
      },
      {
        path: '/orderVerfity',
        component: orderVerfity,
        meta: {
          name: '订单审核',
          icon: 'el-icon-aim',
        },
        children: [
          {
            path: '/orderPassList',
            component: orderPassList,
            meta: {
              name: '审核通过列表',
              icon: 'el-icon-check',
            },
          },
          {
            path: '/orderNoPassList',
            component: orderNoPassList,
            meta: {
              name: '审核未通过列表',
              icon: 'el-icon-close',
            },
          },
        ],
      },
    ],
  },
]
export default allRoutes
