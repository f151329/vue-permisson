// 用户菜单相关
const user = () => import('../views/user')

const userList = () => import('../views/user/userManager/userList.vue')
const userEdit = () => import('../views/user/userManager/userEdit.vue')
// 订单管理相关
const order = () => import('../views/order')
const orderBack = () => import('../views/order/orderBack.vue')
const orderCreate = () => import('../views/order/orderCreate.vue')
const orderVerfity = () => import('../views/order/orderVerfity')
const orderPassList = () =>
  import('../views/order/orderVerfity/orderPassList.vue')
const orderNoPassList = () =>
  import('../views/order/orderVerfity/orderNoPassList.vue')
const home = () => import('../views/home/home.vue')
const allRoutes = [
  {
    path: '/home',
    component: home,
    meta: {
      name: '主页',
      icon: 'el-icon-home',
    },
  },
  {
    path: '/user',
    component: user,
    meta: {
      name: '用户管理',
      icon: 'el-icon-user-solid',
    },
    children: [
      {
        path: '/userEdit',
        component: userEdit,
        meta: {
          name: '用户编辑',
          icon: 'el-icon-edit',
        },
      },
      {
        path: '/userList',
        component: userList,
        meta: {
          name: '用户列表',
          icon: 'el-icon-open',
        },
      },
    ],
  },
  {
    path: '/order',
    component: order,
    meta: {
      name: '订单管理',
      icon: 'el-icon-platform-eleme',
    },
    children: [
      {
        path: '/orderBack',
        component: orderBack,
        meta: {
          name: '订单退货',
          icon: 'el-icon-circle-close',
        },
      },
      {
        path: '/orderCreate',
        component: orderCreate,
        meta: {
          name: '订单创建',
          icon: 'el-icon-circle-plus-outline',
        },
      },
      {
        path: '/orderVerfity',
        component: orderVerfity,
        meta: {
          name: '订单审核',
          icon: 'el-icon-aim',
        },
        children: [
          {
            path: '/orderPassList',
            component: orderPassList,
            meta: {
              name: '审核通过列表',
              icon: 'el-icon-check',
            },
          },
          {
            path: '/orderNoPassList',
            component: orderNoPassList,
            meta: {
              name: '审核未通过列表',
              icon: 'el-icon-close',
            },
          },
        ],
      },
    ],
  },
]
export default allRoutes
